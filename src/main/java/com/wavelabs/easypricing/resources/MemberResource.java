package com.wavelabs.easypricing.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wavelabs.easypricing.services.MemberService;
import com.wavelabs.easypricing.model.Member;
import com.wavelabs.easypricing.model.Message;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@Path("/members")
@Api(value = "/Member", description = "Operations about Members")
public class MemberResource {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Memeber created"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Creates the Member")
	public Response addMember(Member member) {
		return Response.ok(MemberService.addMember(member)).build();

	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Memeber found"),
			@ApiResponse(code = 400, message = "Invalid ID supplied"),
			@ApiResponse(code = 404, message = "Member not found"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Finds the Member by Id")
	public Response getMember(
			@ApiParam(value = "Id of the existing Member to get the Member", required = true) @PathParam("id") int id) {
		Member member = MemberService.getMember(id);
		if(member!=null){
			return Response.status(200).entity(member).build();
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Member not found");
			return Response.status(404).entity(message).build();
		}
	}

	@DELETE
	@Path("/{id}")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Member Successfully removed"),
			@ApiResponse(code = 400, message = "Invalid ID supplied"),
			@ApiResponse(code = 404, message = "Member not found"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Removes the Member by Id")
	public Response deleteMember(
			@ApiParam(value = "Id of the existing Member to delete the Member", required = true) @PathParam("id") int memberId) {
			try{
			MemberService.deleteMember(memberId);
			Message message = new Message();
			message.setId(200);
			message.setMessage("Member deleted successfully!");
			return Response.status(200).entity(message).build();
		} catch(IllegalArgumentException exception) {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Member not found");
			return Response.status(404).entity(message).build();
		}
	}
}
