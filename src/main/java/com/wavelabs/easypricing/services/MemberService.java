package com.wavelabs.easypricing.services;

import com.wavelabs.easypricing.dao.MemberDao;
import com.wavelabs.easypricing.model.Member;

public class MemberService {
	public static Member addMember(Member member) {
		return MemberDao.persistMember(member);
	}

	public static Member getMember(int memberId) {
		return MemberDao.getMember(memberId);
	}

	public static Member deleteMember(int memberId) {
		return MemberDao.deleteMember(memberId);
	}
	
	public static Member updateMember(Member member) {
		return MemberDao.updateMember(member);
	}
}
