package com.wavelabs.easypricing;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wavelabs.easypricing.resources.MemberResource;
import com.wavelabs.easypricing.resources.PlanResource;
import com.wavelabs.easypricing.resources.PromocodeResource;
import com.wavelabs.easypricing.resources.SchemeResource;
import com.wavelabs.easypricing.resources.ZipCodeResource;

import io.swagger.jaxrs.config.BeanConfig;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

@Component
@ApplicationPath("/easypricing/webapi")
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public JerseyConfig(ObjectMapper objectMapper) {
		// register endpoints
		//packages("com.wavelabs.easypricing.resources", "io.swagger.jaxrs.listing");
		// register jackson for json
		register(MemberResource.class);
		register(PlanResource.class);
		register(PromocodeResource.class);
		register(SchemeResource.class);
		register(ZipCodeResource.class);
		register(new ObjectMapperContextResolver(objectMapper));
	}

	@PostConstruct
	public void init() {
		// Register components where DI is needed
		this.configureSwagger();
	}

	@Provider
	public static class ObjectMapperContextResolver implements ContextResolver<ObjectMapper> {

		private final ObjectMapper mapper;

		public ObjectMapperContextResolver(ObjectMapper mapper) {
			this.mapper = mapper;
		}

		@Override
		public ObjectMapper getContext(Class<?> type) {
			return mapper;
		}
	}

	private void configureSwagger() {

		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setTitle("Easy Pricing");
		beanConfig.setVersion("1.0");
		beanConfig.setSchemes(new String[] { "http" });
		beanConfig.setHost("localhost:8080");
		beanConfig.setBasePath("easypricing/webapi/");
		beanConfig.setResourcePackage("com.wavelabs.easypricing");
		beanConfig.setScan(true);
		beanConfig.setDescription("Provides Plans and promo codes.");
	}
}