FROM java:8
ADD target/easypricing.jar easypricing.jar
ENTRYPOINT ["java","-jar","easypricing.jar"]
