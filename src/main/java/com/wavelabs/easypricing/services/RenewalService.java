package com.wavelabs.easypricing.services;

import java.util.Timer;
import java.util.TimerTask;

public class RenewalService extends TimerTask {

	Timer timer;

	public RenewalService() {

	}

	public RenewalService(Timer timer) {
		this.timer = timer;
	}

	public void run() {
		PlanService.renewalPlan();
		timer.cancel();
	}

}
