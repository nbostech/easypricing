package com.wavelabs.easypricing.model;

public enum DiscountType {
	PERCENTAGE, AMOUNT;
}
