package com.wavelabs.easypricing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.wavelabs.easypricing")
public class EasypricingApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(EasypricingApplication.class, args);
	}
}
