package com.wavelabs.easypricing.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wavelabs.easypricing.services.PlanService;
import com.wavelabs.easypricing.model.Message;
import com.wavelabs.easypricing.model.Plan;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/plans")
@Api(value = "/Plan", description = "Operations about Plans")
public class PlanResource {

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Plan found"),
			@ApiResponse(code = 400, message = "Invalid ID supplied"),
			@ApiResponse(code = 404, message = "Plan not found"), @ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Finds the Plan by Id")
	public Response getPlan(
			@ApiParam(value = "Id of the existing Plan to get the Plan", required = true) @PathParam("id") int id) {
		Plan plan = PlanService.getPlan(id);
		if (plan != null) {
			return Response.status(200).entity(plan).build();
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Plan not found");
			return Response.status(404).entity(message).build();
		}
	}

	@GET
	@Path("planafterfreetrial/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Plan after Free Trial found"),
			@ApiResponse(code = 400, message = "Invalid ID supplied"),
			@ApiResponse(code = 404, message = "Plan not found"), @ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Gets the Plan after Free Trial Expiry")
	public Response getPlanAfterFreeTrial(
			@ApiParam(value = "Id of the Free Trial", required = true) @PathParam("id") int id) {
		Plan plan = null;
		try {
			plan = PlanService.getPlanAfterFreeTrial(id);
		} catch (NullPointerException e) {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Free Trial Not Found!");
			return Response.status(404).entity(message).build();
		}
		if (plan != null) {
			return Response.status(200).entity(plan).build();
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Free Trial Not Found!");
			return Response.status(404).entity(message).build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Plan created"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Creates the Plan")
	public Response addPlan(Plan plan,
			@ApiParam(value = "Id of the existing Scheme to add the Plan to a Scheme", required = true) @QueryParam("schemeId") int schemeId) {
		return Response.ok(PlanService.addPlan(plan, schemeId)).build();
	}

	@DELETE
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Plan Successfully removed"),
			@ApiResponse(code = 400, message = "Invalid ID supplied"),
			@ApiResponse(code = 404, message = "Plan not found"), @ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Removes the Plan by Id")
	public String deletePlan(
			@ApiParam(value = "Id of the existing Plan to delete the Plan", required = true) @PathParam("id") int id) {
		PlanService.deletePlan(id);
		return "Plan Successfully deleted";
	}

	@PUT
	@Path("/cancelplan")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Plan or Free Trial successfully Cancelled"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Cancels the Plan or Free Trial of the member")
	public Response cancelPlan(
			@ApiParam(value = "Id of the Member", required = true) @QueryParam("memberId") int memberId) {
		return Response.ok(PlanService.cancelPlan(memberId)).build();

	}

	@PUT
	@Path("/changeplan")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Plan has been changed successfully!"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Changes the Plan of the member")
	public Response changePlan(
			@ApiParam(value = "Id of the Member", required = true) @QueryParam("memberId") int memberId,
			@ApiParam(value = "Id of the Plan", required = true) @QueryParam("planId") int planId) {
		return Response.ok(PlanService.changePlan(memberId, planId)).build();
	}

	@PUT
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Free Trial Subscribed"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Assigns the free trial plan to the member")
	@Path("/freetrialsubscription")
	public Response subscribeFreeTrial(
			@ApiParam(value = "Id of the Scheme", required = true) @QueryParam("schemeId") int schemeId,
			@ApiParam(value = "Id of the Free Trial", required = true) @QueryParam("planId") int planId,
			@ApiParam(value = "Id of the Member", required = true) @QueryParam("memberId") int memberId,
			@ApiParam(value = "Promo Code") @QueryParam("promoCode") String promoCode) {
		return Response.ok(PlanService.freeTrial(schemeId, planId, memberId, promoCode)).build();
	}

	@PUT
	@Path("/plansubscription")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Payment Successful"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Assigns the plan to the member")
	@Produces(MediaType.APPLICATION_JSON)
	public Response makePayment(
			@ApiParam(value = "Amount the member has to pay", required = true) @QueryParam("amount") double amount,
			@ApiParam(value = "Id of the Member", required = true) @QueryParam("memberId") int memberId,
			@ApiParam(value = "Id of the Promo Code member used") @QueryParam("promoId") int promoId,
			@ApiParam(value = "Id of the Plan Member is subscribing", required = true) @QueryParam("planId") int planId,
			@ApiParam(value = "Id of the Scheme", required = true) @QueryParam("schemeId") int schemeId) {
		return Response.ok(PlanService.makePayment(amount, memberId, promoId, planId, schemeId)).build();
	}

	@PUT
	@Path("/renewal")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Renewal Successful"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Renews the plan after expiry date of plan or assigns the plan after the expiry of free trial version")
	@Produces(MediaType.APPLICATION_JSON)
	public Response renewalPlan() {
		PlanService.renewalPlan();
		Message message = new Message();
		message.setId(200);
		message.setMessage("Renewal Successful");
		return Response.status(200).entity(message).build();
	}
}
