package com.wavelabs.easypricing.dao;

import org.hibernate.Session;

import com.wavelabs.easypricing.model.ZipCode;
import com.wavelabs.easypricing.utility.Helper;

public class ZipCodeDao {
	
	public static ZipCode persistZipCode(ZipCode zipCode){
		Session session = Helper.getSession();
		session.save(zipCode);
		session.beginTransaction().commit();
		session.close();
		return zipCode;
	}
	
	public static ZipCode getZipCode(int zipCodeId){
		Session session = Helper.getSession();
		ZipCode zipCode = (ZipCode) session.get(ZipCode.class, zipCodeId);
		return zipCode;
	}
}
