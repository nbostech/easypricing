<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Home</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<h3>Scheme</h3>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Scheme
					Name</label>
				<div class="col-sm-10">
					<input type="text" name="scheme" class="form-control" id="name"
						placeholder="Enter scheme name">
				</div>
			</div>

			<br />
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="button-group">
							Select Zip Codes
							<button type="button"
								class="btn btn-default btn-sm dropdown-toggle"
								data-toggle="dropdown">
								<span class="glyphicon glyphicon-check"></span> <span
									class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="#" class="small" data-value="option1"
									tabIndex="-1"><input type="checkbox" />&nbsp;50001</a></li>
								<li><a href="#" class="small" data-value="option2"
									tabIndex="-1"><input type="checkbox" />&nbsp;50002</a></li>
								<li><a href="#" class="small" data-value="option3"
									tabIndex="-1"><input type="checkbox" />&nbsp;50003</a></li>
								<li><a href="#" class="small" data-value="option4"
									tabIndex="-1"><input type="checkbox" />&nbsp;50004</a></li>
								<li><a href="#" class="small" data-value="option5"
									tabIndex="-1"><input type="checkbox" />&nbsp;50005</a></li>
								<li><a href="#" class="small" data-value="option6"
									tabIndex="-1"><input type="checkbox" />&nbsp;50006</a></li>
								<li><a href="#" class="small" data-value="option1"
									tabIndex="-1"><input type="checkbox" />&nbsp;50007</a></li>
								<li><a href="#" class="small" data-value="option2"
									tabIndex="-1"><input type="checkbox" />&nbsp;50008</a></li>
								<li><a href="#" class="small" data-value="option3"
									tabIndex="-1"><input type="checkbox" />&nbsp;50009</a></li>
								<li><a href="#" class="small" data-value="option4"
									tabIndex="-1"><input type="checkbox" />&nbsp;50010</a></li>
								<li><a href="#" class="small" data-value="option5"
									tabIndex="-1"><input type="checkbox" />&nbsp;50011</a></li>
								<li><a href="#" class="small" data-value="option6"
									tabIndex="-1"><input type="checkbox" />&nbsp;50012</a></li>
								<li><a href="#" class="small" data-value="option1"
									tabIndex="-1"><input type="checkbox" />&nbsp;50013</a></li>
								<li><a href="#" class="small" data-value="option2"
									tabIndex="-1"><input type="checkbox" />&nbsp;50014</a></li>
								<li><a href="#" class="small" data-value="option3"
									tabIndex="-1"><input type="checkbox" />&nbsp;50015</a></li>
								<li><a href="#" class="small" data-value="option4"
									tabIndex="-1"><input type="checkbox" />&nbsp;50016</a></li>
								<li><a href="#" class="small" data-value="option5"
									tabIndex="-1"><input type="checkbox" />&nbsp;50017</a></li>
								<li><a href="#" class="small" data-value="option6"
									tabIndex="-1"><input type="checkbox" />&nbsp;50018</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<div class="checkbox">
				<label><input type="checkbox" name="trial"
					value="Include Trial Version">Include Trial Version</label>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<div class="checkbox">
				<label><input type="checkbox" value='Make Active'
					name='active'>Make Active</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default">Submit</button>
		</div>
	</div>
	</form>
	</div>
	<script type="text/javascript">
		var options = [];

		$('.dropdown-menu a')
				.on(
						'click',
						function(event) {

							var $target = $(event.currentTarget), val = $target
									.attr('data-value'), $inp = $target
									.find('input'), idx;

							if ((idx = options.indexOf(val)) > -1) {
								options.splice(idx, 1);
								setTimeout(function() {
									$inp.prop('checked', false)
								}, 0);
							} else {
								options.push(val);
								setTimeout(function() {
									$inp.prop('checked', true)
								}, 0);
							}

							$(event.target).blur();

							console.log(options);
							return false;
						});
	</script>
</body>
</html>