package com.wavelabs.easypricing.model;

import java.util.Date;
import java.util.Set;

public class PromoCode {
	private int id;
	private String code;
	private double discountAmount;
	private Date validFrom;
	private Date expiresOn;
	private DiscountType discountType;
	private Set<Integer> noOfMonthsApplicable;
	

	public PromoCode() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Set<Integer> getNoOfMonthsApplicable() {
		return noOfMonthsApplicable;
	}

	public void setNoOfMonthsApplicable(Set<Integer> noOfMonthsApplicable) {
		this.noOfMonthsApplicable = noOfMonthsApplicable;
	}

	public Date getExpiresOn() {
		return expiresOn;
	}

	public void setExpiresOn(Date expiresOn) {
		this.expiresOn = expiresOn;
	}

	public DiscountType getDiscountType() {
		return discountType;
	}

	public void setDiscountType(DiscountType discountType) {
		this.discountType = discountType;
	}

}
