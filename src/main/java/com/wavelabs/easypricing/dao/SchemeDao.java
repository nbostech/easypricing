package com.wavelabs.easypricing.dao;

import org.hibernate.Session;

import com.wavelabs.easypricing.model.Scheme;
import com.wavelabs.easypricing.utility.Helper;

public class SchemeDao {
	public static Scheme persistScheme(Scheme scheme){
		Session session = Helper.getSession();
		session.save(scheme);
		session.beginTransaction().commit();
		session.close();
		return scheme;
	}
	
	public static Scheme getScheme(int schemeId){
		Session session = Helper.getSession();
		Scheme scheme = (Scheme) session.get(Scheme.class, schemeId);
		return scheme;
	}
}
