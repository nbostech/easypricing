package com.wavelabs.easypricing.services;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

import com.wavelabs.easypricing.dao.PromoCodeDao;
import com.wavelabs.easypricing.model.DiscountType;
import com.wavelabs.easypricing.model.Member;
import com.wavelabs.easypricing.model.Plan;
import com.wavelabs.easypricing.model.PromoCode;
import com.wavelabs.easypricing.utility.Helper;

public class PromocodeService {

	public static PromoCode addPromoCode(PromoCode promoCode) {
		return PromoCodeDao.persistPromoCode(promoCode);
	}

	public static PromoCode getPromoCode(int promoCodeId) {
		return PromoCodeDao.getPromoCode(promoCodeId);
	}

	public static PromoCode updatePromoCode(PromoCode promoCode) {
		return PromoCodeDao.updatePromoCode(promoCode);
	}

	public static double[] getAmount(int planId, String promocode, int memberId) {
		Session session = Helper.getSession();
		double amount = 0;
		double error = 0;
		PromoCode promo = null;
		Query promoQuery = session.createQuery("from PromoCode p where p.code=:promocode");
		promoQuery.setParameter("promocode", promocode);
		promoQuery.setMaxResults(1);
		promo = (PromoCode) promoQuery.uniqueResult();
		if (promo != null) {
			Date expiryDate = promo.getExpiresOn();
			Date date = Calendar.getInstance().getTime();
			DiscountType discountType = promo.getDiscountType();
			Set<Integer> months = promo.getNoOfMonthsApplicable();
			double discount = promo.getDiscountAmount();
			if (expiryDate.after(date)) {
				Member member = MemberService.getMember(memberId);
				Set<PromoCode> promoCodes = member.getUsedPromoCodes();
				if (!(promoCodes.contains(promo))) {
					Plan plan;
					if (PlanService.isPlan(planId)) {
						plan = PlanService.getPlan(planId);
					} else {
						plan = PlanService.getPlanAfterFreeTrial(planId);
					}
					double price = plan.getPrice();
					Integer planMonths = plan.getDurationInMonths();
					if (months.contains(planMonths)) {
						if (discountType.equals(DiscountType.PERCENTAGE)) {
							amount = price - ((discount / 100) * price);
						} else {
							amount = price - discount;
						}
						System.out.println("The amount to pay: " + amount);
						error = 0;
						return new double[] { amount, error };

					} else {
						error = 1;
						return new double[] { amount, error };
					}

				} else {
					error = 2;
					return new double[] { amount, error };
				}
			} else {
				error = 3;
				return new double[] { amount, error };
			}
		} else {
			error = 4;
			return new double[] { amount, error };
		}

	}

}
