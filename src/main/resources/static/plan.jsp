<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Plan Creation</title>
</head>
<body bgcolor="#ecd9c6">
	<div class="container">
		<h2>Create Plan</h2>
		<form class="form-horizontal" action="webapi/plans/add" method="post"
			id="form">
			<div class="form-group">
				Plan Name: <input type="text" class="form-control" name="name"
					placeholder="Enter Plan Name">
			</div>
			<br>
			<div class="form-group">
				Price: <input type="text" class="form-control" name="price"
					placeholder="Enter Plan Price">
			</div>
			<br>
			<div class="form-group">
				Duration in Months: <input type="text" class="form-control"
					name="duration" placeholder="Enter Duration">
			</div>
			<%@ page import="java.util.List"%>
			<%@ page import="java.util.Iterator"%>
			<%@ page
				import="com.wavelabs.PricingPlansAndPromotions.utility.Helper"%>
			<%@ page import="com.wavelabs.PricingPlansAndPromotions.model.Scheme"%>
			<%@ page import="org.hibernate.Query"%>
			<%@ page import="org.hibernate.Session"%>

			<%
				Session session1 = Helper.getSession();
				Query q = session1.createQuery("from Scheme");
				List list = q.list();
			%>
			<br>
			Select a Scheme: <select name="schemeId">
			<option disabled selected value>-- select a scheme --</option>
				<%
					Iterator<Scheme> iterator = list.iterator();
					while (iterator.hasNext()) {
						Scheme scheme = iterator.next();%>
						<option value="<%= scheme.getId() %>"><%= scheme.getName() %></option>
					<% }
				%>
			</select> <br><br>
			<div class="form-group">

				<button type="submit" class="btn btn-default">Submit</button>

			</div>
		</form>
	</div>
</body>
</html>