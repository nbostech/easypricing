package com.wavelabs.easypricing.dao;

import java.util.Set;

import org.hibernate.Session;

import com.wavelabs.easypricing.services.SchemeService;
import com.wavelabs.easypricing.model.Plan;
import com.wavelabs.easypricing.model.Scheme;
import com.wavelabs.easypricing.utility.Helper;

public class PlanDao {
	public static Plan persistPlan(Plan plan, int schemeId) {
		Session session = Helper.getSession();
		Scheme scheme = SchemeService.getScheme(schemeId);
		Set<Plan> plans = scheme.getPlans();
		plans.add(plan);
		session.save(scheme);
		session.beginTransaction().commit();
		session.close();
		return plan;
	}
	
	public static Plan getPlan(int planId){
		Session session = Helper.getSession();
		Plan plan = (Plan) session.get(Plan.class, planId);
		return plan;
	}
	
	public static void deletePlan(int planId){
		Session session = Helper.getSession();
		Plan plan = (Plan) session.get(Plan.class, planId);
		session.delete(plan);
		session.beginTransaction().commit();
		session.close();
	}
}
