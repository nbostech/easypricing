package com.wavelabs.easypricing.services;

import java.util.Set;

import org.hibernate.Session;

import com.wavelabs.easypricing.dao.ZipCodeDao;
import com.wavelabs.easypricing.model.Scheme;
import com.wavelabs.easypricing.model.ZipCode;
import com.wavelabs.easypricing.utility.Helper;

public class ZipCodeService {
	public static ZipCode addZipCode(ZipCode zipCode) {
		return ZipCodeDao.persistZipCode(zipCode);
	}

	public static ZipCode getZipCode(int zipCodeId) {
		return ZipCodeDao.getZipCode(zipCodeId);
	}

	public static ZipCode addSchemeToZip(int schemeId, int zipCodeId) {
		Session session = Helper.getSession();
		ZipCode zipCode = ZipCodeService.getZipCode(zipCodeId);
		Scheme scheme = SchemeService.getScheme(schemeId);
		Set<Scheme> schemes = zipCode.getSchemes();
		schemes.add(scheme);
		session.save(zipCode);
		session.beginTransaction().commit();
		return zipCode;
	}

}
