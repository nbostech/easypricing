package com.wavelabs.easypricing.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wavelabs.easypricing.services.ZipCodeService;
import com.wavelabs.easypricing.model.Message;
import com.wavelabs.easypricing.model.ZipCode;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/zipcodes")
@Api(value = "/Zip Code", description = "Operations about Zip codes")
public class ZipCodeResource {

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Zip Code found"),
			@ApiResponse(code = 400, message = "Invalid ID supplied"),
			@ApiResponse(code = 404, message = "Zip Code not found"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Finds the Zip Code by Id")
	public Response getZipCode(@ApiParam(value = "Id of the Zip Code", required = true) @PathParam("id") int id) {
		ZipCode zipCode = ZipCodeService.getZipCode(id);
		if (zipCode != null) {
			return Response.status(200).entity(zipCode).build();
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Zip Code not found");
			return Response.status(404).entity(message).build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Zip Code created"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Creates the Zip Code")
	public Response addZipCode(ZipCode zip) {
		return Response.ok(ZipCodeService.addZipCode(zip)).build();

	}

	@PUT
	@Path("/assignscheme")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Scheme has been assigned to Zip code"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Assigns the scheme to the Zip Code")
	public Response addSchemeToZip(
			@ApiParam(value = "Id of the Scheme", required = true) @QueryParam("schemeId") int schemeId,
			@ApiParam(value = "Id of the Zip Code", required = true) @QueryParam("zipCodeId") int zipCodeId) {
		return Response.ok(ZipCodeService.addSchemeToZip(schemeId, zipCodeId)).build();
	}
}
