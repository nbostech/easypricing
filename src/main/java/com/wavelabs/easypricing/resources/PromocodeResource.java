package com.wavelabs.easypricing.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wavelabs.easypricing.services.PromocodeService;
import com.wavelabs.easypricing.model.Message;
import com.wavelabs.easypricing.model.PromoCode;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/promocodes")
@Api(value = "/Promo Code", description = "Operations about Promo Codes")
public class PromocodeResource {

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Promo Code found"),
			@ApiResponse(code = 400, message = "Invalid ID supplied"),
			@ApiResponse(code = 404, message = "Promo Code not found"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Finds the Promo Code by Id")
	public Response getPromoCode(@ApiParam(value = "Id of the Promo Code", required = true) @PathParam("id") int id) {
		PromoCode promoCode = PromocodeService.getPromoCode(id);
		if (promoCode != null) {
			return Response.status(200).entity(promoCode).build();
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Promo Code not found");
			return Response.status(404).entity(message).build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Promo Code created"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Creates the Promo Code")
	public Response addPromoCode(PromoCode code) {
		return Response.ok(PromocodeService.addPromoCode(code)).build();
	}

	@GET
	@Path("/getamount")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Amount Calculated"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Gets the amount to pay after applying Promo code")
	public Response getAmount(@ApiParam(value = "Id of the Plan", required = true) @QueryParam("planId") int planId,
			@ApiParam(value = "Promo code which the member applied", required = true) @QueryParam("promocode") String promocode,
			@ApiParam(value = "Id of the Member", required = true) @QueryParam("memberId") int memberId) {
		try {

			double[] result = PromocodeService.getAmount(planId, promocode, memberId);
			double amount = result[0];
			double error = result[1];
			if (error == 0) {
				Message message = new Message();
				message.setId(200);
				message.setMessage("The amount you have to pay: Rs." + amount);
				return Response.status(200).entity(message).build();
			} else if (error == 1) {
				Message message = new Message();
				message.setId(200);
				message.setMessage("The Promo code you have entered is not applicable for your Plan!");
				return Response.status(200).entity(message).build();
			} else if (error == 2) {
				Message message = new Message();
				message.setId(200);
				message.setMessage("Sorry you have already used this promo code!");
				return Response.status(200).entity(message).build();
			} else if (error == 3) {
				Message message = new Message();
				message.setId(200);
				message.setMessage("The Promo code you have entered is expired!");
				return Response.status(200).entity(message).build();
			} else {
				Message message = new Message();
				message.setId(200);
				message.setMessage("The Promo Code you have entered doesn't exist!");
				return Response.status(200).entity(message).build();
			}

		} catch (NullPointerException ne) {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Please Check the Promo Code You Entered!");
			return Response.status(404).entity(message).build();
		}
	}

}
