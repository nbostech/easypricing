package com.wavelabs.easypricing.dao;

import org.hibernate.Session;

import com.wavelabs.easypricing.model.Member;
import com.wavelabs.easypricing.utility.Helper;

public class MemberDao {

	public static Member persistMember(Member member) {
		Session session = Helper.getSession();
		session.save(member);
		session.beginTransaction().commit();
		session.close();
		return member;

	}

	public static Member getMember(int memberId) {
		Session session = Helper.getSession();
		Member member = (Member) session.get(Member.class, memberId);
		return member;
	}

	public static Member deleteMember(int memberId) {
		Session session = Helper.getSession();
		Member member = (Member) session.get(Member.class, memberId);
		session.delete(member);
		session.beginTransaction().commit();
		session.close();
		return member;
	}

	public static Member updateMember(Member member) {
		Session session = Helper.getSession();
		session.saveOrUpdate(member);
		session.beginTransaction().commit();
		return member;
	}
}
