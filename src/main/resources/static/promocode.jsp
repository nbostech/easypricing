<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Promo code Creation</title>
</head>
<body>
	<div class="container">
		<h2>Create Promo code</h2>
		<form class="form-horizontal" action="webapi/promocodes/add"
			method="post">
			<div class="form-group">
				Promo Code: <input type="text" class="form-control" name="code"
					placeholder="Enter Promo code">
			</div>
			<div class="form-group">
				Discount Type <select name="dtype">
					<option disabled selected value>-- select option --</option>
					<option value="percentage">Percentage</option>
					<option value="amount">Amount</option>
				</select>
			</div>
			<div class="form-group">
				Discount value: <input type="text" class="form-control"
					name="dvalue" placeholder="Enter value">
			</div>
			<div class="form-group">
				Valid From: <input type="text" data-date-inline-picker="true"
					class="form-control" name="startdate">
			</div>
			<div class="form-group">
				Expires on: <input type="text" data-date-inline-picker="true"
					class="form-control" name="enddate">
			</div>
			<div class="form-group">
				Applicable to Plans of Months: <select name="months">
					<option disabled selected value>-- select option --</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
				</select>
				<div class="form-group">

					<button type="submit" class="btn btn-default">Submit</button>

				</div>
		</form>
	</div>
</body>
</html>