package com.wavelabs.easypricing.dao;

import org.hibernate.Session;

import com.wavelabs.easypricing.model.PromoCode;
import com.wavelabs.easypricing.utility.Helper;

public class PromoCodeDao {
	public static PromoCode persistPromoCode(PromoCode promoCode) {
		Session session = Helper.getSession();
		session.save(promoCode);
		session.beginTransaction().commit();
		session.close();
		return promoCode;
	}

	public static PromoCode getPromoCode(int promoCodeId) {
		Session session = Helper.getSession();
		PromoCode promoCode = (PromoCode) session.get(PromoCode.class, promoCodeId);
		return promoCode;
	}
	
	public static PromoCode updatePromoCode(PromoCode promoCode) {
		Session session = Helper.getSession();
		session.saveOrUpdate(promoCode);
		session.beginTransaction().commit();
		session.close();
		return promoCode;
	}

	public static void deletePromoCode(int promoCodeId) {
		Session session = Helper.getSession();
		PromoCode promoCode = (PromoCode) session.get(PromoCode.class, promoCodeId);
		session.delete(promoCode);
		session.beginTransaction().commit();
		session.close();
	}
}
