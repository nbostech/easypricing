<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Scheme</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body bgcolor="#d6f5f5">

	<div class="container">
		<h2>Create Scheme</h2>
		<form class="form-horizontal" action="webapi/schemes/add"
			method="post" id="form">
			<div class="form-group">
				Scheme Name: <input type="text" class="form-control" name="name"
					placeholder="Enter Scheme Name">
			</div>
			<div>
			<br>
			Free Trial <select name="freetrial">
			<option disabled selected value>-- select option --</option>
				<option value="yes">yes</option>
				<option value="no">no</option>
			</select><br><br>
			</div> 
			
			<div>
			Active <select name="active">
			<option disabled selected value>-- select option --</option>
				<option value="yes">yes</option>
				<option value="no">no</option>
			</select>
			</div>
			<br>
			<div class="form-group">

				<button type="submit" class="btn btn-default">Submit</button>

			</div>
		</form>
	</div>
</body>
</html>