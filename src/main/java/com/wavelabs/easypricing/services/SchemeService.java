package com.wavelabs.easypricing.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.wavelabs.easypricing.dao.SchemeDao;
import com.wavelabs.easypricing.model.Scheme;
import com.wavelabs.easypricing.model.ZipCode;

public class SchemeService {

	public static Scheme addScheme(Scheme scheme) {
		return SchemeDao.persistScheme(scheme);
	}

	public static Scheme getScheme(int schemeId) {
		return SchemeDao.getScheme(schemeId);
	}

	public static Scheme getRandomScheme(int zipCodeId) {
		ZipCode zip = ZipCodeService.getZipCode(zipCodeId);
		Set<Scheme> schemes = zip.getSchemes();
		List<Scheme> list = new ArrayList<Scheme>();
		list.addAll(schemes);
		Collections.shuffle(list);
		Scheme scheme = (Scheme) list.get(0);
		return scheme;

	}

	public static void main(String[] args) {
		getRandomScheme(1);
	}
}
