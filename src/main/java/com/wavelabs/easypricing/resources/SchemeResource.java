package com.wavelabs.easypricing.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wavelabs.easypricing.services.SchemeService;
import com.wavelabs.easypricing.model.Message;
import com.wavelabs.easypricing.model.Scheme;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/schemes")
@Api(value = "/Scheme", description = "Operations about Schemes")
public class SchemeResource {

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Scheme found"),
			@ApiResponse(code = 400, message = "Invalid ID supplied"),
			@ApiResponse(code = 404, message = "Scheme not found"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Finds the Scheme by Id")
	public Response getScheme(@ApiParam(value = "Id of the Scheme", required = true) @PathParam("id") int schemeId) {
		Scheme scheme = SchemeService.getScheme(schemeId);
		if (scheme != null) {
			return Response.status(200).entity(scheme).build();
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Scheme not found");
			return Response.status(404).entity(message).build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Scheme created"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Creates the Scheme")
	public Response addScheme(Scheme scheme) {
		return Response.ok(SchemeService.addScheme(scheme)).build();

	}

	@GET
	@Path("/randomscheme")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Random Scheme Found"),
			@ApiResponse(code = 400, message = "Invalid Zip Code supplied"),
			@ApiResponse(code = 404, message = "Zip Code not found"),
			@ApiResponse(code = 500, message = "Server Error") })
	@ApiOperation(value = "Randomly selects the scheme based on Zip Code")
	public Response getRandomScheme(
			@ApiParam(value = "Id of the Zip Code", required = true) @QueryParam("zipCodeId") int zipCodeId) {
		try {
			Scheme scheme = SchemeService.getRandomScheme(zipCodeId);
			return Response.status(200).entity(scheme).build();

		} catch (NullPointerException ne) {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Zip Code Not Found!");
			return Response.status(404).entity(message).build();
		}
	}

}
