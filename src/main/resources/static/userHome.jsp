<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Home</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<script type="text/javascript">
		function getData() {
			$
					.ajax({
						type : 'GET',
						url : 'http://localhost:8099/PricingPlansAndPromotions/webapi/schemes/random?zip=666777',
						dataType : 'json',
						success : function(response) {
							$('#lblData').json(response);
						},
						error : function(error) {
							console.log(error);
						}
					});
		}
	</script>
	<button id="buttton" onclick="getData()" class="btn btn-success">Get Plans</button>
	<h3 id="lblData">Click on button to see your plans</h3>
</body>
</html>