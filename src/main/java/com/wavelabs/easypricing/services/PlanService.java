package com.wavelabs.easypricing.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

import com.wavelabs.easypricing.dao.PlanDao;
import com.wavelabs.easypricing.model.Member;
import com.wavelabs.easypricing.model.Plan;
import com.wavelabs.easypricing.model.PromoCode;
import com.wavelabs.easypricing.model.Scheme;
import com.wavelabs.easypricing.utility.Helper;

public class PlanService {

	public static Plan addPlan(Plan plan, int schemeId) {
		return PlanDao.persistPlan(plan, schemeId);
	}

	public static Plan getPlan(int planId) {
		return PlanDao.getPlan(planId);
	}

	public static void deletePlan(int planId) {
		PlanDao.deletePlan(planId);
	}

	public static boolean isPlan(int planId) {
		Plan plan = PlanService.getPlan(planId);
		if (plan.getPrice() == 0 && plan.getDurationInMonths() == 0) {
			return false;
		} else {
			return true;
		}
	}

	public static Scheme getScheme(int planId) {
		Session session = Helper.getSession();
		Query parent = session.createQuery("select s from Scheme s join s.plans p where p.id=:planId");
		parent.setParameter("planId", planId);
		Scheme scheme = (Scheme) parent.uniqueResult();
		return scheme;
	}

	public static Plan getPlanAfterFreeTrial(int planId) {
		Session session = Helper.getSession();
		if (!(isPlan(planId))) {
			int schemeId = getScheme(planId).getId();
			Query lessPricePlan = session
					.createQuery("select p from Scheme s inner join s.plans p where s.id=:schemeId ORDER BY p.price");
			lessPricePlan.setParameter("schemeId", schemeId);
			lessPricePlan.setFirstResult(1);
			lessPricePlan.setMaxResults(1);
			Plan plan = (Plan) lessPricePlan.uniqueResult();
			return plan;
		} else {
			return null;
		}
	}

	public static Member freeTrial(int schemeId, int planId, int memberId, String code) {
		Session session = Helper.getSession();
		Query codeQuery = session.createQuery("select p.id from PromoCode p where p.code=:code");
		codeQuery.setParameter("code", code);
		int promoCodeId = (int) codeQuery.uniqueResult();
		PromoCode promoCode = PromocodeService.getPromoCode(promoCodeId);
		Plan plan = PlanService.getPlan(planId);
		Plan nextPlan = getPlanAfterFreeTrial(planId);
		int noOfDays = plan.getNoOfDaysForFreeTrial();
		Date expiryDate = DateService.addDaysToDate(noOfDays);
		Member member = MemberService.getMember(memberId);
		member.setPlan(plan);
		member.setNextSubscriptionPlan(nextPlan);
		member.setFreeTrialExpiryDate(expiryDate);
		member.setFreeTrialPromoCode(promoCode);
		MemberService.updateMember(member);
		return member;
	}

	public static Member makePayment(double amount, int memberId, int promoCodeId, int planId, int schemeId) {
		PromoCode promoCode = PromocodeService.getPromoCode(promoCodeId);
		String code = promoCode.getCode();
		Member member = MemberService.getMember(memberId);
		double[] amountCalculated = PromocodeService.getAmount(planId, code, memberId);
		double billAmount = amountCalculated[0];
		if (billAmount > 0) {
			Plan plan = PlanService.getPlan(planId);
			if (amount == billAmount) {
				member.setPlan(plan);
				int noOfDays = plan.getDurationInMonths() * 30;
				Date planRenewalDate = DateService.addDaysToDate(noOfDays);
				member.setPlanRenewalDate(planRenewalDate);
				member.setAmountPaidForPresentPlan(amount);
				Set<PromoCode> promoCodes = member.getUsedPromoCodes();
				promoCodes.add(promoCode);
				MemberService.updateMember(member);
			} else {
				System.out.println("Transaction Failed!");
			}
		} else {
			System.out.println("Something went wrong!");
		}
		return member;
	}

	public static Member cancelPlan(int memberId) {
		Member member = MemberService.getMember(memberId);
		Plan plan = member.getPlan();
		int planId = plan.getId();
		if (isPlan(planId)) {
			member.setPlan(null);
			member.setPlanRenewalDate(null);
			member.setAmountPaidForPresentPlan(0);
			MemberService.updateMember(member);
			return member;
		} else {
			member.setPlan(null);
			member.setNextSubscriptionPlan(null);
			member.setFreeTrialExpiryDate(null);
			member.setFreeTrialPromoCode(null);
			MemberService.updateMember(member);
			return member;
		}
	}

	public static Member changePlan(int memberId, int planId) {
		Member member = MemberService.getMember(memberId);
		Plan plan = PlanService.getPlan(planId);
		int noOfDays = plan.getDurationInMonths() * 30;
		Date newPlanRenewalDate = DateService.addDaysToDate(noOfDays);
		member.setPlan(plan);
		member.setPlanRenewalDate(newPlanRenewalDate);
		member.setAmountPaidForPresentPlan(plan.getPrice());
		MemberService.updateMember(member);
		return member;

	}

	public static void renewalPlan() {
		Session session = Helper.getSession();
		@SuppressWarnings("unchecked")
		List<Member> members = session.createQuery("from Member").list();
		for (Member member : members) {
			if (member.getPlan() != null) {
				Plan plan = member.getPlan();
				int planId = plan.getId();
				Date today = Calendar.getInstance().getTime();
				if (isPlan(planId)) {
					Date planRenewalDate = member.getPlanRenewalDate();
					if (today.after(planRenewalDate)) {
						int noOfDays = plan.getDurationInMonths() * 30;
						Date newPlanRenewalDate = DateService.addDaysToDate(noOfDays);
						member.setPlanRenewalDate(newPlanRenewalDate);
						MemberService.updateMember(member);
					}
				} else {
					Date expiry = member.getFreeTrialExpiryDate();
					if (today.after(expiry)) {
						Plan nextPlan = member.getNextSubscriptionPlan();
						PromoCode promoCode = member.getFreeTrialPromoCode();
						String code = promoCode.getCode();
						double[] amountCalculated = PromocodeService.getAmount(nextPlan.getId(), code, member.getId());
						double price = amountCalculated[0];
						if (price > 0) {
							member.setFreeTrialExpiryDate(null);
							member.setPlan(nextPlan);
							member.setNextSubscriptionPlan(null);
							member.setFreeTrialPromoCode(null);
							int noOfDays = nextPlan.getDurationInMonths() * 30;
							Date planRenewalDate = DateService.addDaysToDate(noOfDays);
							member.setPlanRenewalDate(planRenewalDate);
							member.setAmountPaidForPresentPlan(price);
							MemberService.updateMember(member);
						} else {
							System.out.println("Something has gone wrong!");
						}

					}
				}
			}
		}
	}
}
